from unittest import TestCase
from math import isinf, inf, isnan, nan
from calculator.safe_eval import safe_power, safe_divide


class TestSafeOperations(TestCase):

    def test_safe_power(self):
        cases = [
            (1010.0, 1010.0, inf),
            (-1000.0, 1001.0, -inf),
            (-1000.0, 1000.0, inf),
            (2.0, 2.0, 4.0),
            (2.0, -1.0, 0.5),
            (99.0, 99.0, 99.0 ** 99.0),
            (-1, 0.5, nan),
            (0, 0, nan)
        ]

        for (a, b, target) in cases:
            result = safe_power(a, b)
            if isinf(target):
                self.assertTrue(isinf(result))
                if result > 0:
                    self.assertGreater(result, 0)
                else:
                    self.assertLess(result, 0)
            elif isnan(target):
                self.assertTrue(isnan(result), f'{a}**{b}: expected {target}, got {result}')
            else:
                self.assertEqual(result, target, f'{a}**{b}: expected {target}, got {result}')

    def test_safe_divide(self):
        cases = [
            (1, 2, 0.5),
            (1, 0, nan),
            (0, 0, nan)
        ]

        for (a, b, target) in cases:
            result = safe_divide(a, b)
            if isnan(target):
                self.assertTrue(isnan(result))
            else:
                self.assertEqual(result, target)
