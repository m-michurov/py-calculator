from unittest import TestCase
from math import isnan, nan, isinf, inf

from calculator.safe_eval import evaluate


class TestEvaluate(TestCase):

    def test_evaluate(self):
        cases = [
            ('2^2^2', 16),
            ('1+1', 2),
            ('-1', -1),
            ('2+2*2', 6),
            ('(2+2)*2', 8),
            ('(-1)^0.5', nan),
            ('0/0', nan),
            ('1/0', nan),
            ('((((((((((((+7))))))))))))', 7),
            ('4^0.5', 2),
            ('1000^1000^1000', inf),
            ('-1000^999', -inf)
        ]

        for (expression, target) in cases:
            result = evaluate(expression)
            if isnan(target):
                self.assertTrue(isnan(result), f'"{expression}": expected {target}, got {result}')
            elif isinf(target):
                self.assertTrue(isinf(result), f'"{expression}": expected {target}, got {result}')
            else:
                self.assertEqual(result, target, f'"{expression}": expected {target}, got {result}')
