from unittest import TestCase

from calculator.expression_buffer import ExpressionBuffer


class TestExpressionBuffer(TestCase):

    def test_append(self):
        cases = [
            ('.', '0.'),
            ('1', '1'),
            ('e', '0'),
            ('*', '0'),
            ('/', '0'),
            ('^', '0'),
            ('%', '0'),
            ('+', '+'),
            ('-', '-'),
            ('1+2*3', '1+2*3'),
            ('1.0%3.0+-*/.7', '1.0%3.0/.7'),
            ('1.0/3.0+-*%^.7', '1.0/3.0^.7'),
            ('+-*/', '-'),
            ('1.0.1', '1.01'),
            ('1,0', '1.0'),
            ('79-.0.^63.66', '79-.0^63.66'),
            ('79-.0.+*%63.66', '79-.0%63.66'),
            ('.......', '0.'),
            ('-(+(-(+(-(+(', '-(+(-(+(-(+('),
            ('(((-+-+-(((())))))()))(', '(((-(((((('),
            ('1(2.3', '12.3'),
            ('(.22+(22.)*369.7)', '(.22+(22.)*369.7)'),
            ('+)-+-+(-+-+)+*/(-+-+)/-(-++-)%*%/*(/*', '+(+(-(-('),
            ('0000000', '0'),
            ('000.0000', '0.0000'),
            ('1.0e10', '1.0e+10'),
            ('1.0e+10', '1.0e+10'),
            ('1.0e-10', '1.0e-10'),
            ('1e5', '1e+5'),
            ('1.e5', '1.e+5'),
            ('1.e-5', '1.e-5'),
            ('1e1^1e1', '1e+1^1e+1'),
            ('.0e10', '0.0e+10'),
            ('.0e+10', '0.0e+10'),
            ('.0e-10', '0.0e-10'),
            ('e5', '5'),
            ('.e5', '0.e+5'),
            ('.e-5', '0.e-5'),
            ('e1^e1', '1^1'),
            ('1+.0e10', '1+.0e+10'),
            ('1+.0e+10', '1+.0e+10'),
            ('1+.0e-10', '1+.0e-10'),
            ('1+e5', '1+5'),
            ('1+.e5', '1+.e+5'),
            ('1+.e-5', '1+.e-5'),
            ('1+e1^e1', '1+1^1')
        ]

        for (input_sequence, target) in cases:
            buffer = ExpressionBuffer()
            for char in input_sequence:
                buffer.append(char)

            result = str(buffer)
            self.assertEqual(target, str(buffer), f'{input_sequence}: expected {target}, got {result}')

    def test_append_reset(self):
        cases = [
            ('1+2*3', '1.0/3.0+-*/.7', '1.0/3.0/.7'),
            ('1.0/3.0+-*/.7', '1.0/3.0+-*/^.7', '1.0/3.0^.7'),
            ('1.0/3.0+-*/^.7', '+*/', '1.0/3.0^.7/'),
            ('+-*/', '1.0.1',  '-1.01'),
            ('2.0.1', '1,0', '1.0'),
            ('1,0', '79-.0.^63.66', '79-.0^63.66'),
            ('1,0', '79-.0.^63.66', '79-.0^63.66'),
            ('1,0', '(79-.0.^63.66)', '(79-.0^63.66)'),
            ('79-.0.^63.66', '79-.0.+*/63.66', '79-.0/63.66')
        ]

        for (input_sequence, new_expression, target) in cases:
            buffer = ExpressionBuffer(input_sequence)
            buffer.append(new_expression[0], reset_if_expression=True)
            for char in new_expression[1:]:
                buffer.append(char)

            result = str(buffer)
            self.assertEqual(
                target, str(buffer), f'{input_sequence}, {new_expression}: expected {target}, got {result}')

    def test_back(self):
        cases = [
            ('*b', '0'),
            ('1+b2', '12'),
            ('1+2*3/+*-4.0bbbbbbbbb', '0'),
            ('1+2*3/+*-4.0bbbbbbbbb42', '42')
        ]

        for (input_sequence, target) in cases:
            buffer = ExpressionBuffer()
            for char in input_sequence:
                if char == 'b':
                    buffer.back()
                else:
                    buffer.append(char)

            result = str(buffer)
            self.assertEqual(target, str(buffer), f'{input_sequence}: expected {target}, got {result}')

    def test_clear(self):
        cases = [
            ('42', '0'),
            ('', '0')
        ]

        for (input_sequence, target) in cases:
            buffer = ExpressionBuffer()
            for char in input_sequence:
                buffer.append(char)

            buffer.clear()

            result = str(buffer)
            self.assertEqual(target, str(buffer), f'{input_sequence}: expected {target}, got {result}')

    def test_reset(self):
        cases = [
            ('42', '1+2*3', '1+2*3'),
            ('', '42', '42'),
            ('2+2*2', '', '0'),
            ('1+2*3', ')()', '(')
        ]

        for (input_sequence, new_sequence, target) in cases:
            buffer = ExpressionBuffer()
            for char in input_sequence:
                buffer.append(char)

            buffer.reset(new_sequence)

            result = str(buffer)
            self.assertEqual(target, str(buffer), f'{input_sequence}: expected {target}, got {result}')
