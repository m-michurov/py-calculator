from math import inf, nan, sqrt

MAX_BASE = 100
MAX_POWER = 100


def safe_power(base: float, power: float) -> float:
    base = float(base)
    power = float(power)
    if power > MAX_POWER:
        if power % 2 == 0:
            return inf
        elif base < 0 and power % 2 != 0:
            return -inf

    if abs(base) > MAX_BASE:
        if power > MAX_POWER:
            return inf if power % 2 == 0 else -inf

    if base == 0 and power == 0:
        return nan

    result = base ** power
    return result if type(result) is not complex else nan


def safe_divide(num: float, divisor: float) -> float:
    return nan if divisor == 0 else num / divisor


def safe_sqrt(num: float) -> float:
    return nan if num < 0 else sqrt(num)
