from math import nan
import ast
import operator as op
from typing import Optional, Union

from calculator.safe_eval.operations import safe_power, safe_divide


OPERATORS = {
    ast.Add: op.add,
    ast.Sub: op.sub,
    ast.Mult: op.mul,
    ast.Div: safe_divide,
    ast.Pow: safe_power,
    ast.UAdd: op.pos,
    ast.USub: op.neg,
    ast.Mod: op.mod
}


def parse(expression: str) -> Optional[ast.expr]:
    try:
        return ast.parse(expression.replace('^', '**'), mode='eval').body
    except SyntaxError:
        return None


def evaluate_node(node: ast.expr) -> Union[float, int]:
    if isinstance(node, ast.Num):
        value = node.value
        if type(value) is complex:
            value = value.real
        return value
    elif isinstance(node, ast.UnaryOp):
        return OPERATORS[type(node.op)](evaluate_node(node.operand)) if type(node.op) in OPERATORS else nan
    elif isinstance(node, ast.BinOp):
        return OPERATORS[type(node.op)](evaluate_node(node.left), evaluate_node(node.right)) \
            if type(node.op) in OPERATORS else nan
    else:
        return nan


def evaluate(expression: str) -> Union[int, float]:
    root = parse(expression)
    if not root:
        return nan
    result = evaluate_node(root)
    return int(result) if type(result) is float and result.is_integer() else result
