from enum import Enum, auto

STANDARD_DECIMAL_SEPARATOR = '.'
STANDARD_EXPONENT = 'e'

DIGITS = {str(i) for i in range(10)}
DECIMAL_SEPARATORS = {'.', ','}
EXP = {'e', 'E'}
NUMBER_START_CHARS = DIGITS | DECIMAL_SEPARATORS
NUMBER_CHARS = NUMBER_START_CHARS | EXP

UNARY_OP_CHARS = {'+', '-'}
BINARY_ONLY_OP_CHARS = {'*', '/', '^', '%'}
BINARY_OP_CHARS = {'+', '-'} | BINARY_ONLY_OP_CHARS
OP_CHARS = UNARY_OP_CHARS | BINARY_OP_CHARS

OPENING_BRACKET = '('
CLOSING_BRACKET = ')'
BRACKETS = {OPENING_BRACKET, CLOSING_BRACKET}

EXPRESSION_START_CHARS = NUMBER_CHARS | UNARY_OP_CHARS | {OPENING_BRACKET}
EXPRESSION_END_CHARS = NUMBER_CHARS | {CLOSING_BRACKET}
ALLOWED_CHARS = NUMBER_CHARS | UNARY_OP_CHARS | BINARY_ONLY_OP_CHARS | {OPENING_BRACKET, CLOSING_BRACKET}

MAX_BUFFER_LENGTH = 10000


def number_has_decimal_separator(buffer: list[str]) -> bool:
    for i in range(len(buffer) - 1, -1, -1):
        char = buffer[i]
        if char in DECIMAL_SEPARATORS:
            return True
        if i >= 2 and ''.join(buffer[i - 2:i]) in {'e+', 'e-'}:
            return True
        if char not in DIGITS:
            return False

    return False


def number_has_exponent(buffer: list[str]) -> bool:
    for i in range(len(buffer) - 1, -1, -1):
        char = buffer[i]
        if char in EXP:
            return True
        if char in DECIMAL_SEPARATORS:
            return False
        if char not in DIGITS:
            return False

    return False


def all_zeroes(buffer: list[str]) -> bool:
    for i in range(len(buffer) - 1, -1, -1):
        char = buffer[i]
        if char in DIGITS and char != '0' or char not in DIGITS:
            return False

    return True


class State(Enum):
    LAST_EMPTY_BUFFER = auto()
    LAST_BINARY_OP = auto()
    LAST_UNARY_OP = auto()
    LAST_OPENING_BRACKET = auto()
    LAST_CLOSING_BRACKET = auto()
    LAST_NUMBER = auto()
    LAST_EXPONENT = auto()


class ExpressionBuffer:

    def __init__(self, expression: str = '') -> None:
        self.states = [State.LAST_EMPTY_BUFFER]
        self.buffer: list[str] = []
        self.reset(expression)

    def append(self, char: str, reset_if_expression=False) -> bool:
        """
        Append a single character to expression buffer if the character is valid.

        >>> buffer = ExpressionBuffer()
        >>> buffer.append('1')
        >>> str(buffer)
        1
        >>> buffer.append(')')
        >>> str(buffer)
        1

        :param char: a character to append
        :param reset_if_expression: reset buffer if `char` is a valid first character of an expression
        :return: True if `char` was appended and False otherwise
        """
        if char not in ALLOWED_CHARS or len(self.buffer) >= MAX_BUFFER_LENGTH:
            return False

        state = self.states[-1]
        next_state = None

        if state is State.LAST_EMPTY_BUFFER or state is State.LAST_OPENING_BRACKET:
            if char == OPENING_BRACKET:
                next_state = State.LAST_OPENING_BRACKET
            elif char in NUMBER_START_CHARS:
                if state is not State.LAST_EMPTY_BUFFER or char != '0':
                    next_state = State.LAST_NUMBER
            elif char in UNARY_OP_CHARS:
                next_state = State.LAST_UNARY_OP

        elif state is State.LAST_BINARY_OP:
            if char == OPENING_BRACKET:
                next_state = State.LAST_OPENING_BRACKET
            elif char in NUMBER_START_CHARS:
                next_state = State.LAST_NUMBER
            elif char in BINARY_OP_CHARS:
                self.back()
                next_state = State.LAST_BINARY_OP

        elif state is State.LAST_UNARY_OP:
            if char == OPENING_BRACKET:
                next_state = State.LAST_OPENING_BRACKET
            elif char in NUMBER_START_CHARS:
                next_state = State.LAST_NUMBER
            elif char in UNARY_OP_CHARS:
                self.back()
                next_state = State.LAST_UNARY_OP

        elif state is State.LAST_CLOSING_BRACKET or state is State.LAST_NUMBER:
            if char == CLOSING_BRACKET \
                    and self.states.count(State.LAST_OPENING_BRACKET) > self.states.count(State.LAST_CLOSING_BRACKET):
                next_state = State.LAST_CLOSING_BRACKET
            if char in BINARY_OP_CHARS:
                next_state = State.LAST_BINARY_OP
            if char in NUMBER_CHARS:
                if reset_if_expression:
                    self.clear()
                    next_state = State.LAST_NUMBER
                elif char in DECIMAL_SEPARATORS:
                    if not number_has_decimal_separator(self.buffer):
                        char = STANDARD_DECIMAL_SEPARATOR
                        next_state = State.LAST_NUMBER
                elif char in EXP:
                    if not number_has_exponent(self.buffer):
                        char = STANDARD_EXPONENT
                        next_state = State.LAST_EXPONENT
                elif char == '0':
                    if not all_zeroes(self.buffer):
                        next_state = State.LAST_NUMBER
                else:
                    next_state = State.LAST_NUMBER
            if char == OPENING_BRACKET and reset_if_expression:
                self.clear()
                next_state = State.LAST_OPENING_BRACKET

        if state is State.LAST_EXPONENT:
            if char in UNARY_OP_CHARS:
                next_state = State.LAST_NUMBER
            if char in DIGITS:
                self.append('+')
                next_state = State.LAST_NUMBER

        if next_state:
            self.__append(char, next_state)

        return True if next_state else False

    def append_expression(self, expression: str) -> bool:
        if not self.__can_start_expression():
            return False

        appended = False
        for char in expression:
            appended |= self.append(char)
        return appended

    def reset(self, expression: str) -> None:
        """
        Clears buffer and appends every character of `expression` in order of iteration.

        :param expression: expression to be stored in this buffer.
        :return: None
        """
        self.clear()
        for char in expression:
            self.append(char)

    def clear(self) -> None:
        """
        Removes all characters from buffer.

        :return: None
        """
        self.states = [State.LAST_EMPTY_BUFFER]
        self.buffer = ['0']

    def back(self) -> None:
        """
        Removes the last character. Does nothing if buffer is empty.

        :return: None
        """
        if self.states[-1] is not State.LAST_EMPTY_BUFFER:
            self.buffer.pop(len(self.buffer) - 1)
            self.states.pop(len(self.states) - 1)

        if self.states[-1] is State.LAST_EMPTY_BUFFER and not self.buffer:
            self.buffer.append('0')

    def __append(self, char: str, next_state: State) -> None:
        if self.states[-1] is State.LAST_EMPTY_BUFFER and char != STANDARD_DECIMAL_SEPARATOR:
            self.buffer = [char]
        else:
            self.buffer.append(char)
        self.states.append(next_state)

    def __can_start_expression(self) -> bool:
        state = self.states[-1]
        return state in {
            State.LAST_EMPTY_BUFFER,
            State.LAST_BINARY_OP,
            State.LAST_UNARY_OP,
            State.LAST_OPENING_BRACKET
        }

    def __str__(self) -> str:
        return ''.join(self.buffer)

    def __repr__(self) -> str:
        return str(self)
