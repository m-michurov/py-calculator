import tkinter as tk
from math import isnan, isinf
from tkinter import ttk
from typing import Union
import numpy as np

from calculator.expression_buffer import ExpressionBuffer
from calculator.safe_eval import evaluate

BACKSPACE = 8
ENTER = 13

AC = 'AC'
C = 'C'
EVALUATE = '='
EXP = 'EXP'
ANS = 'ANS'

WIDTH = 350
HEIGHT = 375

LARGE_NUMBER = 1e10
SMALL_NUMBER = 1e-10


def fmt(num: Union[int, float]) -> str:
    if abs(num) > LARGE_NUMBER or abs(num) < SMALL_NUMBER and num != 0:
        return np.format_float_scientific(float(num), trim='0')

    if type(num) is int:
        return str(num)
    return np.format_float_positional(num)


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        self.buffer = ExpressionBuffer('0')
        self.result = 0
        self.new_expression = True

        self.title('Calculator')
        self.geometry(f'{WIDTH}x{HEIGHT}')
        self.style = ttk.Style(self)

        self.bind('<KeyPress>', lambda e: self.on_key_pressed(e.keycode, e.char))
        self.bind_all('<<Paste>>', lambda _: self.on_paste(self.clipboard_get()))

        self.prev = tk.StringVar(value='')
        result = ttk.Label(self, textvariable=self.prev, anchor='e')
        result.pack(padx=10, pady=5, fill='both')

        self.input = tk.StringVar(value=str(self.buffer))
        expression = ttk.Label(self, textvariable=self.input, anchor='e', font='16')
        expression.pack(padx=10, fill='both')

        buttons = [
            [EXP, ANS, AC, C],
            ['(', ')', '%', '^'],
            ['7', '8', '9', '/'],
            ['4', '5', '6', '*'],
            ['1', '2', '3', '-'],
            ['0', '.', EVALUATE, '+'],
        ]

        buttons_panel = ttk.Frame(self)
        buttons_panel.pack(fill='both', padx=5, pady=5, expand=True)

        row = 0
        for buttons_row in buttons:
            if len(buttons_row) != 1:
                buttons_panel.grid_rowconfigure(row, weight=1)

            column = 0
            for button_label in buttons_row:
                column += 1
                buttons_panel.grid_columnconfigure(column, weight=1)

                button = ttk.Button(
                    buttons_panel, text=button_label, command=lambda label=button_label: self.on_button_pressed(label))
                button.grid(row=row, column=column, sticky='nesw')
            row += 1

    def on_button_pressed(self, label: str) -> None:
        if label == AC:
            self.buffer.reset('0')
            self.update()
            self.new_expression = True
        elif label == C:
            self.on_backspace()
        elif label == EVALUATE:
            self.on_enter()
        elif label == EXP:
            self.buffer.append('e')
            self.update()
        elif label == ANS:
            if self.new_expression:
                self.buffer.reset(fmt(self.result))
                self.update()
            else:
                appended = self.buffer.append_expression(fmt(self.result))
                if appended:
                    self.update()
        else:
            self.on_key_pressed(char=label)

    def on_key_pressed(self, key_code: int = 0, char: str = '') -> None:
        if key_code == BACKSPACE:
            self.on_backspace()
        elif key_code == ENTER:
            self.on_enter()
        else:
            appended = self.buffer.append(char, reset_if_expression=self.new_expression)
            if appended:
                self.update()

    def on_paste(self, new_expression: str) -> None:
        self.buffer.reset(new_expression)
        self.update()

    def on_backspace(self) -> None:
        self.buffer.back()
        self.update()

    def on_enter(self) -> None:
        if self.new_expression:
            return
        self.new_expression = True

        expression = str(self.buffer)
        self.prev.set(f'{expression} = ')
        self.result = evaluate(expression)

        formatted_result = fmt(self.result)
        self.buffer.reset(formatted_result)
        self.input.set(formatted_result if not isnan(self.result) and not isinf(self.result) else str(self.result))

    def update(self) -> None:
        expression = str(self.buffer)
        self.input.set(expression)

        if self.new_expression:
            self.prev.set(f'Ans = {fmt(self.result)}')
            self.new_expression = False
