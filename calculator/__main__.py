import sys
from calculator.gui import App

EXIT_SUCCESS = 0


def main(_: list[str] = None) -> int:
    app = App()
    app.mainloop()
    return EXIT_SUCCESS


sys.exit(main())
